<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Lottery;
use App\Page;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HelpController extends Controller
{
    public function index()
    {
        $lotteries = Lottery::all();
        
        foreach ($lotteries as $lottery) {
            // Eager load closing times for each row
            $lottery->closing_times()->get(['title', 'time']);

            // Format the time (from 14:21:00 to 14:21)
            $lottery->closing_times = $lottery->closing_times->map(function ($closing_time) {
                $closing_time->time = Carbon::parse($closing_time->time)->format('H:i');
                return $closing_time;
            });
        }

        // Rep Dom
        $currentTime = Carbon::now()->format('H:i');

        // Current time in Madrid, Spain
        $currentTimeMadrid = Carbon::now('Europe/Madrid')->format('H:i');

        $helpPage = Page::where('name', 'help')->first();

        return view('guest.help', compact('lotteries', 'currentTime', 'currentTimeMadrid', 'helpPage'));
    }
}
