<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $quiniela
 * @property int $pale
 * @property int $super_pale
 * @property int $tripleta
 */
class SalesLimit extends Model
{
    /**
     * Entre todas las ventas del día no se puede exceder este límite.
     */
    const DEFAULT_DAILY_GLOBAL_ID = 1;

    /**
     * Ningún vendedor debe exceder este límite semanal.
     * Aplica para cada uno, a menos que un seller tenga un límite específico aplicado a su user_id.
     */
    const DEFAULT_WEEKLY_SELLER_ID = 2;
    
    protected $fillable = [
        'quiniela', 'pale', 'super_pale', 'tripleta', 
        'user_id',
        'frequency', 'lottery_level'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
