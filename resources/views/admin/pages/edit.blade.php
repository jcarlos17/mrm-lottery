@extends('layouts.app')

@section('styles')
    <link href="//cdn.jsdelivr.net/npm/quill@2.0.2/dist/quill.snow.css" rel="stylesheet" />
@endsection

@section('content')
    <div class="container">
        <div class="card mb-3">
            <div class="card-header">
                Página de ayuda
            </div>
            <div class="card-body">
                <div id="editor">
                    {!! $helpPage->html_content !!}
                </div>
                <button class="btn btn-primary mt-2" id="btnSave">
                    Guardar cambios
                </button>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="//cdn.jsdelivr.net/npm/quill@2.0.2/dist/quill.js"></script>
    <script>
        const toolbarOptions = [
            ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
            // ['link'],
            [{ 'list': 'ordered'}, { 'list': 'bullet' }],
            // [{ 'header': [1, 2, 3, 4, 5, 6, false] }],            
            ['clean']                                         // remove formatting
        ];
        
        const quill = new Quill('#editor', {
            modules: {
                toolbar: toolbarOptions
            },
            theme: 'snow'
        });
        
        $('#btnSave').on('click', () => {
            const htmlContent = quill.getSemanticHTML();

            fetch('/admin/pages/help', {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                body: JSON.stringify({ html_content: htmlContent })
            })
            .then(response => response.json())
            .then(data => {
                console.log('Success:', data);
                alert('Cambios guardados con éxito');
            })
            .catch(error => {
                console.error('Error:', error);
                alert('Error inesperado, por favor actualice la página');
            });
        });
    </script>
@endsection