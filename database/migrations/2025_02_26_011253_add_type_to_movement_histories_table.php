<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeToMovementHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movement_histories', function (Blueprint $table) {
            $table->string('type')->after('id')->nullable(); // 'transfer', 'ticket_sold', 'winner_reward', 'ticket_voided'
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movement_histories', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
