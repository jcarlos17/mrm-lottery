<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Raffle extends Model
{
    protected $fillable = [
        'number_1',
        'number_2',
        'number_3',
        'lottery_id',
        'datetime'
    ];
    
    public function lottery(): Relation
    {
        return $this->belongsTo(Lottery::class);
    }

    public function winners(): Relation
    {
        return $this->hasMany(Winner::class);
    }
}
