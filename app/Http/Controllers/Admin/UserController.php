<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\MovementHistory;
use App\User;
use App\Winner;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = User::withTrashed()
            ->where('role', User::SELLER)
            ->orderBy('deleted_at')
            ->get();

        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:256',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6'
        ];
        $messages = [

            'name.required' => 'Es indispensable ingresar el nombre completo.',
            'name.max' => 'El nombre del vendedor es demasiado extenso.',
            'email.required' => 'Es indispensable ingresar el e-mail del vendedor.',
            'email.email' => 'El e-mail ingresado no es válido.',
            'email.max' => 'El e-mail es demasiado extenso.',
            'email.unique' => 'El e-mail ya se encuentra en uso.',
            'password.required' => 'Es necesario ingresar una contraseña',
            'password.min' => 'La contraseña debe presentar al menos 6 caracteres.',
        ];
        $this->validate($request, $rules, $messages);

        $seller = new User();
        $seller->name = $request->input('name');
        $seller->email = $request->input('email');
        $seller->password = bcrypt($request->input('password'));
        $seller->commission = $request->input('commission');
        $seller->save();

        return redirect('/users')->with('notification', 'El vendedor se ha registrado exitosamente.');
    }

    public function edit($id)
    {
        $user = User::withTrashed()->findOrFail($id);

        return view('admin.users.edit', compact('user'));
    }

    public function update($id, Request $request)
    {
        $rules = [
            'name' => 'required|max:256',
            'email' => "required|email|max:255|unique:users,email,$id",
            'password' => 'nullable|min:6',
            'commission' => 'nullable|numeric'
        ];
        $messages = [
            'name.required' => 'Es indispensable ingresar el nombre completo.',
            'name.max' => 'El nombre del vendedor es demasiado extenso.',
            'email.required' => 'Es indispensable ingresar el e-mail del vendedor.',
            'email.email' => 'El e-mail ingresado no es válido.',
            'email.max' => 'El e-mail es demasiado extenso.',
            'email.unique' => 'El e-mail ya se encuentra en uso.',
            'password.min' => 'La contraseña debe presentar al menos 6 caracteres.',
        ];
        $this->validate($request, $rules, $messages);

        $user = User::withTrashed()->findOrFail($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $password = $request->input('password');
        if ($password) {
            $user->password = bcrypt($password);
        }

        if ($user->balance == 0) {
           $user->commission = $request->input('commission');
        }

        $user->save();

        return redirect('/users')->with('notification', 'Usuario modificado exitosamente.');
    }

    public function deactivate($id)
    {
        $evaluator = User::findOrFail($id);
        $evaluator->delete();

        return back()->with('notification', 'El usuario se ha eliminado correctamente.');
    }

    public function activate($id)
    {
        $userDeleted = User::withTrashed()->findOrFail($id);
        $userDeleted->restore();

        return back()->with('notification', 'El usuario con sus respectivos procesos se ha restaurado correctamente.');
    }

    public function showBalance($id)
    {
        $user = User::withTrashed()->findOrFail($id);
        
        $tokenResult = $user->createToken('Personal Access Token');
        
        $pendingToPay = Winner::where('paid', false)
            ->where('user_id', $user->id)
            ->sum('reward');

        $central = $user->balance + $pendingToPay;

        // Calcular desde el último ajuste
        $lastTransferDate = MovementHistory::where('user_id', $user->id)
            ->where('type', MovementHistory::TYPE_TRANSFER)
            ->latest('created_at')
            ->value('created_at');

        // Últimos movimientos del historial
        $lastMovements = $user->movement_histories()
            ->when($lastTransferDate, function ($query) use ($lastTransferDate) {
                return $query->where('created_at', '>', $lastTransferDate);
            })
            ->get();

        return view('admin.users.balance', compact(
            'user', 'tokenResult', 'pendingToPay', 'central', 'lastMovements'
        ));
    }
}
