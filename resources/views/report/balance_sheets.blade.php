@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card mb-3">
            <div class="card-header">
                Balance de cuenta
            </div>
            
            <div class="card-body text-center">
                <div class="row">
                    <div class="col-12 col-sm-4" title="Porcentaje que se le cobra al vendedor">
                        <p class="mb-0"><strong>Comisión</strong></p>
                        <h5>$ {{ number_format($commissionPaid, 2, '.', '') }}</h5>
                    </div>
                    <div class="col-12 col-sm-4" title="Central = Balance + pendiente por pagar">
                        <p class="mb-0"><strong>Central</strong></p>
                        <h5>$ {{ number_format($central, 2, '.', '') }}</h5>
                    </div>
                    <div class="col-12 col-sm-4" title="Cantidad de tickets vendidos">
                        <p class="mb-0"><strong>Venta Total</strong></p>
                        <h5>$ {{ number_format($soldTotal, 2, '.', '') }}</h5>
                    </div>
                </div>
            </div>
            
            <div class="card-footer text-center">
                <div title="Pendiente por pagar">
                    <strong>Premiado:</strong>
                    <h5 class="d-inline">$ {{ number_format($pendingToPay, 2, '.', '') }}</h5>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Últimos movimientos
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Descripción</th>
                            <th>Cantidad</th>
                            <th>Fecha</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($lastMovements as $movementHistory)
                            <tr>
                                <td>{{ $movementHistory->description }}</td>
                                <td>{{ $movementHistory->amount }}</td>
                                <td>{{ $movementHistory->created_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
@endsection
