<?php

namespace App\Http\Controllers;

use App\TicketPlay;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ListController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $id = $request->id;
        $sellerId = $request->seller_id;
        $fromDate = $request->from_date;
        $toDate = $request->to_date;
        $option = $request->option;
        
        $sellers = User::get([
            'id', 'name'
        ]);

        $query = TicketPlay::with('ticket');

        if (auth()->user()->is_role(User::SELLER)) {
            $query = $query->whereHas('ticket', function ($query) {
                $query->where('user_id', auth()->id());
            });
        }

        if ($option && $option == 'ID') {
            $query = $query->whereHas('ticket', function ($query) use ($id) {
                $query->where('code', $id);
            });
        } elseif ($option && in_array($option, ['Range', 'Seller'])) {
            $carbonFrom = new Carbon($fromDate);
            $carbonTo = new Carbon($toDate);

            $query = $query->whereBetween('created_at', [
                $carbonFrom->startOfDay(), 
                $carbonTo->endOfDay()
            ]);
            
            if ($option === 'Seller') {
                $query->whereHas('ticket', function ($query) use ($sellerId) {
                    $query->where('user_id', $sellerId);
                });
            }
        } elseif ($option && $option == 'Date') {
            $query = $query->whereDate('created_at', $fromDate);
        }

        $ticketPlays = $query
            ->orderByDesc('created_at')
            ->paginate(20);

        $totalSum = 0;
        foreach ($ticketPlays as $play) {
            $totalSum += $play->points * $play->ticket->lotteries->count();
        }

        return view('lists', compact(
            'option', 'sellers',
            'id', 'fromDate', 'toDate', 'sellerId',
            'ticketPlays', 'totalSum'
        ));
    }
}
