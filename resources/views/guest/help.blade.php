@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="card mb-3">
                    <div class="card-header">
                        Ayuda
                    </div>
                    <div class="card-body">
                        <p>En esta página encontrarás información de ayuda sobre las loterías.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card mb-3">
                    <div class="card-header">
                        Hora actual
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Hora Dominicana</th>
                                <th>Madrid, España</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $currentTime }}</td>
                                <td>{{ $currentTimeMadrid }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="card mb-3">
            <div class="card-header">
                Horarios de cierre
            </div>
            <div class="card-body">
                @foreach($lotteries as $lottery)
                    <h3>{{ $lottery->name }} ({{ $lottery->abbreviated }})</h3>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                @foreach($lottery->closing_times as $closing_time)
                                    <th>{{ mb_substr($closing_time->title, 0, 3) }}</th>
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                @foreach($lottery->closing_times as $closing_time)
                                    <td>{{ $closing_time->time }}</td>
                                @endforeach
                            </tr>
                            </tbody>
                        </table>
                    </div>                    
                @endforeach
            </div>
        </div>
        
        <div class="card mb-3">
            <div class="card-header">
                Acerca de los premios
            </div>
            <div class="card-body">
                @if (isset($helpPage))
                    {!! $helpPage->html_content !!}
                @endif

                <p><a href="#" onclick="window.print()">Click para Imprimir Condiciones.</a></p>
            </div>
        </div>
    </div>
@endsection
