@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card mb-3">
            <div class="card-header">
                Detalles de ventas
            </div>
            <div class="card-body">
                <form class="form-inline" role="search">
                    <div class="form-row">
                        <div class="col">
                            <select id="option" class="form-control" name="option">
                                <option value="">Seleccionar una opción</option>
                                <option value="ID" {{ $option == 'ID' ? 'selected' : '' }}>Buscar por ID</option>
                                <option value="Date" {{ $option == 'Date' ? 'selected' : '' }}>Buscar por fecha</option>
                                <option value="Range" {{ $option == 'Range' ? 'selected' : '' }}>Buscar por rango de fechas</option>
                                <option value="Seller" {{ $option == 'Seller' ? 'selected' : '' }}>Buscar por vendedor y rango de fechas</option>
                            </select>
                        </div>
                        <div class="col" id="colId" style="display: {{ $option == 'ID' ?:'none' }}">
                            <input type="text" class="form-control" id="id" name="id" value="{{ $id }}" placeholder="Ingrese un ID de Ticket">
                        </div>
                        <div class="col" id="colSeller" style="{{ $option == 'Seller' ? '' : 'display:none' }}">
                            <select name="seller_id" id="seller_id" class="form-control">
                                <option value="">Seleccionar vendedor</option>
                                @foreach ($sellers as $seller)
                                    <option value="{{ $seller->id }}" {{ $seller->id == $sellerId ? 'selected' : '' }}>
                                        {{ $seller->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col" id="colFromDate" style="display: {{ in_array($option, ['Date', 'Range', 'Seller']) ?:'none' }}">
                            <input type="date" class="form-control" id="from_date" name="from_date" value="{{ $fromDate }}">
                        </div>
                        <div class="col" id="colToDate" style="display: {{ in_array($option, ['Range', 'Seller']) ?:'none' }}">
                            <input type="date" class="form-control" id="to_date" name="to_date" value="{{ $toDate }}">
                        </div>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                @if($option == 'ID' && $id && count($ticketPlays) > 0)
                    Ticket # {{ $id }}:
                    <a class="btn btn-sm btn-primary" href="{{ url('tickets/create?copyTicket='.$id) }}">
                        Nueva venta
                    </a>
                @endif
                <div class="table-responsive mt-3">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th title="Código del ticket">Ticket</th>
                            <th>Lotería</th>
                            <th>Jugada</th>
                            <th>Número</th>
                            <th>Puntos</th>
                            <th>FechaCompra</th>
                            @if(auth()->user()->is_role(\App\User::ADMIN))
                                <th>Vendedor</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($ticketPlays as $play)
                            <tr>
                                <td>{{ $play->ticket->code }}</td>
                                <td>
                                    @foreach($play->ticket->lotteries as $lottery)
                                        {{ $lottery->name }}{{ !$loop->last ? ', ' : '' }}
                                    @endforeach
                                </td>
                                <td>{{ $play->type }}</td>
                                <td>{{ $play->number }}</td>
                                <td title="{{ $play->points }} puntos comprados en cada una de las {{ $play->ticket->lotteries->count() }} loterías del ticket">
                                    {{ $play->points * $play->ticket->lotteries->count() }}
                                </td>
                                <td>{{ $play->created_at }}</td>
                                @if(auth()->user()->is_role(\App\User::ADMIN))
                                    <td>{{ $play->ticket->user->name }}</td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="3">Total Puntos</td>
                            <td>{{ $totalSum }}</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                {{ $ticketPlays->appends(Request::except('page')) }}

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        const $option = $('#option');
        const $colId = $('#colId');
        const $colSeller = $('#colSeller');
        const $colFromDate = $('#colFromDate');
        const $colToDate = $('#colToDate');
        const $id = $('#id');
        const $sellerSelect = $('#seller_id');
        const $fromDate = $('#from_date');
        const $toDate = $('#to_date');

        $option.on('change', function () {
            const val = $(this).val();

            $id.attr('required', val === 'ID');
            $sellerSelect.attr('required', val === 'Seller');
            $fromDate.attr('required', ['Range', 'Date', 'Seller'].includes(val));
            $toDate.attr('required', ['Range', 'Seller'].includes(val));

            $colId.hide();
            $colSeller.hide();
            $colFromDate.hide();
            $colToDate.hide();
            
            if (val === 'ID') {
                $colId.show();
            } else if (val === 'Range') {
                $colFromDate.show();
                $colToDate.show();
            } else if (val === 'Date') {
                $colFromDate.show();
                $toDate.val('');
            } else if (val === 'Seller') {
                $colSeller.show();
                $colFromDate.show();
                $colToDate.show();
            } else {
                $fromDate.val('');
                $toDate.val('');
            }
        });
    </script>
@endsection

