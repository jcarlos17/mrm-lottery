<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use App\Ticket;
use Illuminate\Http\Request;

class WinnerController extends Controller
{
    public function index(Request $request)
    {
        $user = auth()->user();
        $query = $user->winners();

        // Eager load ticket_play and its related ticket
        $query = $query->with('ticket_play.ticket');

        $tokenResult = $user->createToken('Personal Access Token');
        $id = $request->id;
        $notification = '';
        
        if ($id) {
            $ticket = Ticket::where('code', $id)->first();

            if (!$ticket) {
                $winners = [];
                $notification = 'No existe el ticket';

                return view('seller.winners.index', compact(
                    'winners', 'tokenResult', 'id', 'notification'
                ));
            }

            $ticketPlayIds = $ticket->plays()
                ->pluck('id')
                ->toArray();

            $query = $query->whereIn('ticket_play_id', $ticketPlayIds);
        }

        $winners = $query->get();

        return view('seller.winners.index', compact(
            'winners', 'tokenResult', 'id', 'notification'
        ));
    }
}
