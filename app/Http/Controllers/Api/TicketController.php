<?php

namespace App\Http\Controllers\Api;

use App\Earning;
use App\Lottery;
use App\MovementHistory;
use App\SalesLimit;
use App\SalesPlayLimit;
use App\Ticket;
use App\TicketLottery;
use App\TicketPlay;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class TicketController extends Controller
{
    public function index(Request $request)
    {
        $startDate = $request->start_date;
        $endingDate = $request->ending_date;
        $user = $request->user();

        $query = Ticket::query();

        if ($user->is_role(2)) {
            $query = $query->where('user_id', $user->id);
        }

        if ($startDate && $endingDate) {
            $carbonStartDate = Carbon::createFromFormat('Y-m-d', $startDate)->startOfDay();
            $carbonEndingDate = Carbon::createFromFormat('Y-m-d', $endingDate)->endOfDay();
            $query = $query->whereBetween('created_at', [$carbonStartDate, $carbonEndingDate]);
        }

        $totalPoints = $query->sum('total_points');

        $tickets = $query
            ->with('lotteries')
            ->orderBy('created_at', 'desc')->get([
                'id', 'code', 'total_points', 'commission_earned', 'user_id', 'created_at'
            ]);

        $data['tickets'] = $tickets;
        $data['totalPoints'] = $totalPoints;

        return $data;
    }

    public function show(Ticket $ticket): array
    {
        $plays = $ticket->plays()->get([
            'number', 'points', 'type'
        ]);

        unset($ticket->lotteries);
        unset($ticket->plays);
        unset($ticket->updated_at);
        unset($ticket->deleted_at);

        return compact('ticket', 'plays');
    }

    public function store(Request $request)
    {
        // Get params
        $user = $request->user();
        $lotteryIds = $request->input('lotteries');
        $plays = $request->input('plays');

        // New registrations are available by intervals, each day
        $now = Carbon::now();
        // $nameDay = Carbon::now()->format('l');

        if (!$lotteryIds || sizeof($lotteryIds)===0) {
            $data['success'] = false;
            $data['error_message'] = "Es necesario seleccionar al menos una lotería";
            return $data;
        }

        if (!$plays || sizeof($plays)===0) {
            $data['success'] = false;
            $data['error_message'] = "Es necesario ingresar al menos una jugada";
            return $data;
        }

        foreach ($plays as $play) {
            $type = $play['type'];
            $number = $play['number'];
            $ticketPlayPoints = $play['points'];

            if ($ticketPlayPoints <= 0) {
                $data['success'] = false;
                $data['error_message'] = "Los puntos a registrar en la jugada no pueden ser negativos o cero";
                return $data;
            }

            if (!is_numeric($number)) {
                $data['success'] = false;
                $data['error_message'] = "Los números a registrar en la jugada tienen que ser numéricos";
                return $data;
            }

            if ($type == TicketPlay::TYPE_QUINIELA && strlen($number) != 2) {
                $data['success'] = false;
                $data['error_message'] = "Sólo se admiten númeross de 2 dígitos para Quiniela";
                return $data;
            }

            if ($type == TicketPlay::TYPE_PALE && strlen($number) != 4) {
                $data['success'] = false;
                $data['error_message'] = "Sólo se admiten númeross de 4 dígitos para Pale";
                return $data;
            }

            if ($type == TicketPlay::TYPE_TRIPLETA && strlen($number) != 6) {
                $data['success'] = false;
                $data['error_message'] = "Sólo se admiten númeross de 6 dígitos para Tripleta";
                return $data;
            }
        }

        $startOfWeek = Carbon::now()->startOfWeek(Carbon::MONDAY);
        $endOfWeek = Carbon::now()->endOfWeek(Carbon::SUNDAY);
        $startOfDay = Carbon::now()->startOfDay();
        $endOfDay = Carbon::now()->endOfDay();

        // Cada vendedor en todas sus ventas de la semana no puede exceder este límite específico
        $weeklyLimit = SalesLimit::where('user_id', $user->id)
            ->where('lottery_level', 'all')
            ->where('frequency', 'weekly')
            ->first();

        // Cada vendedor en sus ventas del día, para cada lotería, no puede exceder este límite específico
        $dailyLimit = SalesLimit::where('user_id', $user->id)
            ->where('lottery_level', 'each')
            ->where('frequency', 'daily')
            ->first();
        
        $global = SalesLimit::find(SalesLimit::DEFAULT_DAILY_GLOBAL_ID);
        $countLotteryIds = count($lotteryIds);

        if (!$weeklyLimit) {
            // Cuando no se especifica, este es el límite a aplicar por cada vendedor
            $weeklyLimit = SalesLimit::find(SalesLimit::DEFAULT_WEEKLY_SELLER_ID);
        }            

        foreach ($plays as $play) {
            $type = $play['type'];
            $ticketPlayPoints = $play['points'];
            $number = $play['number'];

            // Ticket
            $ticketLimit = SalesPlayLimit::where('type', SalesPlayLimit::TICKET)
                ->where('number', $number)
                ->first(['points']);

            if ($ticketLimit && $ticketLimit->points < $ticketPlayPoints * $countLotteryIds) {
                $data['success'] = false;
                $data['error_message'] = "Lo sentimos, pero excedió el límite de ventas por ticket para el el N° $number ($ticketLimit->points puntos disponibles).";
                return $data;
            }
            
            // Seller
            $sellerLimit = SalesPlayLimit::where('type', SalesPlayLimit::SELLER)
                ->where('number', $number)
                ->first(['points']);

            if ($sellerLimit) {
                $sellerTicketIds = $user->tickets()
                    ->whereBetween('created_at', [$startOfDay, $endOfDay])
                    ->pluck('id');

                $sellerSumPoints = TicketPlay::whereIn('ticket_id', $sellerTicketIds)
                    ->where('number', $number)
                    ->select('points')
                    ->sum('points');

                if ($sellerLimit->points < ($sellerSumPoints + $ticketPlayPoints*$countLotteryIds)) {
                    $data['success'] = false;
                    $available = $sellerLimit->points - $sellerSumPoints;
                    $data['error_message'] = "Lo sentimos, pero excedió su límite de ventas diarías para el N° $number ($available puntos disponibles).";
                    return $data;
                }
            }

            // Global
            $globalLimit = SalesPlayLimit::where('type', SalesPlayLimit::GLOBAL)
                ->where('number', $number)
                ->first(['points']);

            if ($globalLimit) {
                $globalTicketIds = Ticket::whereBetween('created_at', [$startOfDay, $endOfDay])
                    ->pluck('id');

                $globalSumPoints = TicketPlay::whereIn('ticket_id', $globalTicketIds)
                    ->where('number', $number)
                    ->select('points')
                    ->sum('points');

                if ($globalLimit->points < ($globalSumPoints + $ticketPlayPoints*$countLotteryIds)) {
                    $data['success'] = false;
                    $available = $globalLimit->points - $globalSumPoints;
                    $data['error_message'] = "Lo sentimos, pero excedió el límite global de ventas diarías para el N° $number ($available puntos disponibles).";
                    return $data;
                }
            }

            // Check by seller user Weekly
            $weeklyValidationError = $this->validateWeeklySellerLimit(
                $user, $startOfWeek, $endOfWeek, $type, $weeklyLimit, $ticketPlayPoints, $countLotteryIds
            );
            
            if (!is_null($weeklyValidationError)) {
                return $weeklyValidationError;
            }
            
            // Check by seller user Daily
            $dailyValidationError = $this->validateDailySellerLimit(
                $user, $startOfDay, $endOfDay, $type, $dailyLimit, $ticketPlayPoints, $number, $lotteryIds
            );
            
            if (!is_null($dailyValidationError)) {
                return $dailyValidationError;
            }
            
            // global
            $ticketGlobalIds = Ticket::whereBetween('created_at', [$startOfWeek, $endOfWeek])->pluck('id');
            $sumGlobalType = TicketPlay::whereIn('ticket_id', $ticketGlobalIds)
                ->where('type', $type)
                ->select('points')
                ->sum('points');

            if ($type == TicketPlay::TYPE_QUINIELA && $global->quiniela < ($sumGlobalType + $ticketPlayPoints*$countLotteryIds)) {
                $data['success'] = false;
                $available = $global->quiniela - $sumGlobalType;
                $data['error_message'] = "Lo sentimos, pero excedió el límite global de ventas semanales para Quiniela ($available puntos disponibles).";
                return $data;
            }

            if ($type == TicketPlay::TYPE_PALE && $global->pale < ($sumGlobalType + $ticketPlayPoints*$countLotteryIds)) {
                $data['success'] = false;
                $available = $global->pale - $sumGlobalType;
                $data['error_message'] = "Lo sentimos, pero excedió el límite global de ventas semanales para Pale ($available puntos disponibles).";
                return $data;
            }

            if ($type == TicketPlay::TYPE_TRIPLETA && $global->tripleta < ($sumGlobalType + $ticketPlayPoints*$countLotteryIds)) {
                $data['success'] = false;
                $available = $global->tripleta - $sumGlobalType;
                $data['error_message'] = "Lo sentimos, pero excedió el límite global de ventas semanales para Tripleta ($available puntos disponibles).";
                return $data;
            }
        }

        foreach ($lotteryIds as $lotteryId) {
            $lottery = Lottery::find($lotteryId);

            if (!$lottery) {
                $data['success'] = false;
                $data['error_message'] = "No existe ninguna lotería con id $lotteryId.";
                return $data;
            }

            $existsInactive = $lottery->inactive_dates()
                ->where('date', $now->format('Y-m-d'))
                ->exists();

            if ($existsInactive) {
                $data['success'] = false;
                $data['error_message'] = "La lotería $lottery->name ya no admite más jugadas en esta fecha. Vuelva a intentarlo el día de mañana.";
                return $data;
            }

            if (!$lottery->active) {
                $data['success'] = false;
                $data['error_message'] = "No se admiten más jugadas en este horario. La lotería '$lottery->name' se encuentra cerrada.";
                return $data;
            }
        }

        // Validation passed
        $countTickets = $user->tickets()
            ->withTrashed()
            ->count();

        $code = $user->id.'_'.($countTickets+1);

        $ticket = new Ticket();
        $ticket->code = $code;
        $ticket->user_id = $user->id;
        $ticket->save();

        foreach ($lotteryIds as $lotteryId) {
            $ticketLottery = new TicketLottery();
            $ticketLottery->ticket_id = $ticket->id;
            $ticketLottery->lottery_id = $lotteryId;
            $ticketLottery->save();
        }

        foreach ($plays as $play) {
            $type = $play['type'];
            $number = $play['number'];
            $ticketPlayPoints = $play['points'];

            $play = new TicketPlay();
            $play->number = $number;
            $play->points = $ticketPlayPoints;
            $play->type = $type;
            $play->ticket_id = $ticket->id;
            $play->save();
        }

        $totalPoints = $ticket->plays()->select('points')->sum('points');
        $countLotteries = $ticket->lotteries()->count();

        // Total points among the selected lotteries
        $ticket->total_points = $totalPoints * $countLotteries;

        // Commission
        $commission = $user->commission ?? User::DEFAULT_COMMISSION;
        $commissionEarned = 100 - $commission;

        // System wins
        $ticket->commission_earned = $totalPoints * $countLotteries * ($commissionEarned/100);
        $ticket->save();

        // Seller wins
        $amount = $ticket->total_points * ($commission/100);

        // Movement
        MovementHistory::create([
            'type' => MovementHistory::TYPE_TICKET_SOLD,
            'description' => 'Ticket N° '.$ticket->code.' registrado',
            'amount' => -$amount,
            'user_id' => $user->id
        ]);
        
        // balance sheets
        $user->balance -= $amount;
        $user->save();

        // Earnings
        Earning::updateOrCreate(
            ['user_id' => $user->id],
            [
                'quantity_tickets' => DB::raw("quantity_tickets + 1"),
                'quantity_points' => DB::raw("quantity_points + ". $ticket->total_points),
                'income' => DB::raw("income + ". $ticket->total_points),
                'commission_earned' => DB::raw("commission_earned + ". $ticket->commission_earned),
            ]
        );

        $data['success'] = true;
        $data['ticket'] = $ticket;
        
        return $data;
    }

    /**
     * Esta función valida el límite de puntos por tipo de jugada, para el seller indicado, en la semana actual.
     * Si ocurre un error de validación, se devuelve la información en formato de array. Otherwise null.
     */
    private function validateWeeklySellerLimit(
        User $user, 
        Carbon $startOfWeek, Carbon $endOfWeek, 
        string $type, 
        SalesLimit $weeklyLimit,
        int $point,
        int $countLotteryIds
    ): ?array
    {
        $weeklyTicketIds = $user
            ->tickets()
            ->whereBetween('created_at', [$startOfWeek, $endOfWeek])
            ->pluck('id');

        $soldPoints = TicketPlay::whereIn('ticket_id', $weeklyTicketIds)
            ->where('type', $type)
            ->select('points')
            ->sum('points');
        
        $newPoints = $soldPoints + $point*$countLotteryIds;

        if ($type == TicketPlay::TYPE_QUINIELA && $weeklyLimit->quiniela < $newPoints) {
            $data['success'] = false;
            $available = $weeklyLimit->quiniela - $soldPoints;
            $data['error_message'] = "Lo sentimos, pero excedió su límite de ventas semanales para Quiniela ($available puntos disponibles).";
            return $data;
        } else if ($type == TicketPlay::TYPE_PALE && $weeklyLimit->pale < $newPoints) {
            $data['success'] = false;
            $available = $weeklyLimit->pale - $soldPoints;
            $data['error_message'] = "Lo sentimos, pero excedió su límite de ventas semanales para Pale ($available puntos disponibles).";
            return $data;
        } else if ($type == TicketPlay::TYPE_TRIPLETA && $weeklyLimit->tripleta < $newPoints) {
            $data['success'] = false;
            $available = $weeklyLimit->tripleta - $soldPoints;
            $data['error_message'] = "Lo sentimos, pero excedió su límite de ventas semanales para Tripleta ($available puntos disponibles).";
            return $data;
        }
        
        return null;
    }

    /**
     * Endpoint específico para verificar límites diarios, y sugerir cuántos puntos jugar,
     * mientras se agregan jugadas, en el form de creación de tickets.
     * 
     * Formato de respuesta: 
     * - success: Indica si el usuario está dentro de los límites, o sea, pasó el check.
     * - available: Indica cuántos puede vender en caso el límite sea excedido.
     */
    public function checkDailyLimit(Request $request): array
    {
        $user = $request->user();
        
        // Información de la jugada que se está intentando registrar
        $type = $request->input('type');
        $number = $request->input('number');
        $points = $request->input('points');
        $lotteryIds = $request->input('lotteries');

        $startOfDay = Carbon::now()->startOfDay();
        $endOfDay = Carbon::now()->endOfDay();

        // Cada vendedor en sus ventas del día, para cada lotería, no puede exceder este límite específico
        $dailyLimit = SalesLimit::where('user_id', $user->id)
            ->where('lottery_level', 'each')
            ->where('frequency', 'daily')
            ->first();
        
        $dailyValidationError = $this->validateDailySellerLimit(
            $user, $startOfDay, $endOfDay, $type, 
            $dailyLimit, $points, $number, 
            $lotteryIds
        );
        
        if (is_null($dailyValidationError)) {
            return [
                'success' => true
            ];
        }
        
        return $dailyValidationError;
    }

    /**
     * Esta función valida el límite de puntos por tipo de jugada y lotería, para el seller indicado, en el día actual.
     * Si ocurre un error de validación, se devuelve información en un array. 
     * Si no se ha excedido el límite, null.
     */
    private function validateDailySellerLimit(
        User $user,
        Carbon $startOfDay, Carbon $endOfDay,
        string $type,
        ?SalesLimit $dailyLimit,
        int $points,
        string $number,
        array $lotteryIds
    ): ?array
    {
        // No todos los vendedores tienen un daily limit asociado
        if (!$dailyLimit) return null;
        
        $maxSoldPointsByLottery = TicketPlay::join('ticket_lottery', 'ticket_plays.ticket_id', '=', 'ticket_lottery.ticket_id')
            ->join('tickets', 'ticket_plays.ticket_id', '=', 'tickets.id')
            ->where('tickets.user_id', $user->id)
            ->whereIn('ticket_lottery.lottery_id', $lotteryIds)
            ->whereBetween('ticket_plays.created_at', [$startOfDay, $endOfDay])
            ->whereBetween('tickets.created_at', [$startOfDay, $endOfDay])
            ->where('ticket_plays.type', $type)
            ->where('ticket_plays.number', $number)
            ->whereNull('tickets.deleted_at')
            ->select('ticket_lottery.lottery_id', DB::raw('SUM(ticket_plays.points) as total_points'))
            ->groupBy('ticket_lottery.lottery_id')
            ->orderByDesc('total_points')
            ->value('total_points');
        
        $newTotalPoints = $maxSoldPointsByLottery + $points;
        
        if ($type == TicketPlay::TYPE_QUINIELA && $newTotalPoints > $dailyLimit->quiniela) {
            $available = $dailyLimit->quiniela - $maxSoldPointsByLottery;
            $message = "Lo sentimos, usted tiene un límite de ventas diario para Quiniela ($available puntos disponibles).";
            return [
                'success' => false,
                'available' => $available,
                'error_message' => $message
            ];
        } else if ($type == TicketPlay::TYPE_PALE && $newTotalPoints > $dailyLimit->pale) {
            $available = $dailyLimit->pale - $maxSoldPointsByLottery;
            $message = "Lo sentimos, usted tiene un límite de ventas diario para Pale ($available puntos disponibles).";
            return [
                'success' => false,
                'available' => $available,
                'error_message' => $message
            ];
        } else if ($type == TicketPlay::TYPE_TRIPLETA && $newTotalPoints > $dailyLimit->tripleta) {
            $available = $dailyLimit->tripleta - $maxSoldPointsByLottery;
            $message = "Lo sentimos, usted tiene un límite de ventas diario para Tripleta ($available puntos disponibles).";
            return [
                'success' => false,
                'available' => $available,
                'error_message' => $message
            ];
        }
        
        return null;
    }

    public function delete($id, Request $request)
    {
        $ticket = Ticket::find($id);

        if (!$ticket) {
            $data['success'] = false;
            $data['error_message'] = "No existe ningún ticket con id $id.";
            return $data;
        }

        if (!$ticket->available_delete) {
            $data['success'] = false;
            $data['error_message'] = "El ticket ya no puede ser eliminado.";
            return $data;
        }

        $user = $ticket->user;

        // earnings
        Earning::updateOrCreate(
            ['user_id' => $user->id],
            [
                'quantity_tickets' => DB::raw("quantity_tickets - 1"),
                'quantity_points' => DB::raw("quantity_points - ". $ticket->total_points),
                'income' => DB::raw("income - ". $ticket->total_points),
                'commission_earned' => DB::raw("commission_earned - ". $ticket->commission_earned),
            ]
        );

        $amount = $ticket->total_points - $ticket->commission_earned;
        
        // Movement to undo the amount
        MovementHistory::create([
            'type' => MovementHistory::TYPE_TICKET_VOIDED,
            'description' => 'Ticket N° '.$ticket->code.' anulado',
            'amount' => $amount,
            'user_id' => $user->id
        ]);
        
        // Update balance
        $user->balance += $amount;
        $user->save();

        $ticket->delete();

        $data['success'] = true;
        return $data;
    }
}
