<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Lottery;
use App\Page;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function edit(Request  $request)
    {
        $defaultHtml = '<p>LOS PREMIOS SON:</p>
            <ul>
                <li>Primera: 65 € por punto</li>
                <li>Segunda: 12 € por punto</li>
                <li>Tercera: 4 € por punto</li>
                <li>Pale1: 1000 € por punto</li>
                <li>Pale2: 100 € por punto</li>
                <li>SuperPale: 2000 € por punto</li>
                <li>Tripleta: 10000 € por punto</li>
                <li>Media Tripleta: 100 € por punto</li>
                <li>Media Tripleta: 100 € por punto</li>
            </ul>

            <p>LOS TICKETS VENDIDOS DESPUÉS DE LA HORA DE CIERRE DE LA LOTERÍA NO SON VÁLIDOS, Y SI SALEN PREMIADOS NO SE PAGARÁN.</p>

            <p>LOS TICKETS PREMIADOS CADUCAN AL 1 MESES.</p>

            <p>SÓLO LAS APUESTAS DE NÚMEROS PODRÁN TENER DOBLE PREMIO. LAS APUESTAS DE PALES NUNCA TENDRÁN MÁS DE UN PREMIO.</p>';
        
        $helpPage = Page::firstOrCreate([
            'name' => 'help'
        ], [
            'html_content' => $defaultHtml
        ]);
        
        return view('admin.pages.edit', compact('helpPage'));
    }
    
    public function update(Request $request): bool
    {
        Page::where('name', 'help')->update($request->only('html_content'));
        
        return true;
    }
}
