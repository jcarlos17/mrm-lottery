<?php

use Illuminate\Support\Facades\Route;

Route::post('/login', 'Api\AuthController@authenticate');

//Route::get('/tickets', 'Api\TicketController@index');

Route::get('/sellers', 'Api\SellerController@index');
Route::get('/sellers/{id}', 'Api\SellerController@seller');

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('signup', 'Api\AuthController@signUp');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('user', 'Api\AuthController@user');
    });
});

Route::group([
    'middleware' => 'auth:api'
], function() {
    // Sales Limits
    Route::get('sales-limit', 'Api\SalesLimitController@show');

    // Return tickets list
    Route::get('/tickets', 'Api\TicketController@index');
    // Show a ticket
    Route::get('/tickets/{ticket}', 'Api\TicketController@show');
        
    // Check daily limits by seller and lottery
    Route::get('/tickets/limits/check/daily', 'Api\TicketController@checkDailyLimit');
    // Register a ticket
    Route::post('/tickets', 'Api\TicketController@store');

    // Delete a ticket by id
    Route::post('/tickets/{id}/delete', 'Api\TicketController@delete');

    // Earnings
    Route::get('earnings', 'Api\UserController@earning');
    Route::get('winners', 'Api\UserController@winners');
    Route::get('paid/{id}', 'Api\WinnerController@pay');

    // Lotteries
    Route::get('lotteries', 'Api\LotteryController@index');

    // Show movements that affected balance
    Route::get('balance_movements', 'Api\BalanceController@index');
    
    // Register balance
    Route::post('balance/{userId}', 'Api\BalanceController@update');
});
