<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Lottery extends Model
{
    protected $appends = [
        'active'
    ];

    public function closing_times()
    {
        return $this->hasMany(ClosingTime::class);
    }

    public function inactive_dates()
    {
        return $this->hasMany(InactiveDate::class);
    }

    public function prizes()
    {
        return $this->hasMany(Prize::class);
    }

    /**
     * Determine whether the lottery can be played at the moment or not.
     * 
     * The 'l' format returns the day name. E.g. 'saturday' and 'sunday'. 
     * 
     * @return bool
     */
    public function getActiveAttribute(): bool
    {
        $now = Carbon::now();

        $todayDayName = $now->format('l');
        $yesterdayDayName = $now->copy()->subDay()->format('l');

        $yesterdayClosing = $this->closing_times()
            ->where('day', $yesterdayDayName)
            ->first();

        $todayClosing = $this->closing_times()
            ->where('day', $todayDayName)
            ->first();

        $yesterdayClosesSince = Carbon::yesterday()->setTimeFromTimeString($yesterdayClosing->time);
        $yesterdayClosesUntil = $yesterdayClosesSince->copy()->addHours(12);
        
        $todayClosesSince = Carbon::today()->setTimeFromTimeString($todayClosing->time);
        $todayClosesUntil = $todayClosesSince->copy()->addHours(12);
        
        $closedByYesterday = ($now >= $yesterdayClosesSince && $now <= $yesterdayClosesUntil);
        $closedByToday = ($now >= $todayClosesSince && $now <= $todayClosesUntil);

        return !$closedByYesterday && !$closedByToday;
    }
}
