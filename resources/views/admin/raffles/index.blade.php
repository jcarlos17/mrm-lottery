@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Listado de sorteos</div>
            <div class="card-body">
                @if(auth()->user()->is_role(\App\User::ADMIN))
                <div class="form-group">
                    <a href="{{ url('raffles/create') }}" class="btn btn-sm btn-success pull-right mb-3">
                        <i class="fa fa-plus"></i>
                        Registrar nuevo sorteo
                    </a>
                </div>
                @endif
                
                <form class="form-inline" role="search">
                    <div class="form-row">
                        <div class="col">
                            <input type="date" class="form-control" id="filter_date" name="filter_date" value="{{ $filterDate }}">
                        </div>
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-search"></i> Buscar
                        </button>
                    </div>
                </form>

                <div class="table-responsive mt-4">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Lotería</th>
                            <th>Primero</th>
                            <th>Segundo</th>
                            <th>Tercero</th>
                            <th>Fecha y hora</th>
                            @if(auth()->user()->is_role(\App\User::ADMIN))
                            <th>Opción</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($raffles as $raffle)
                            <tr>
                                <td>{{ $raffle->lottery->name }}</td>
                                <td>{{ $raffle->number_1 }}</td>
                                <td>{{ $raffle->number_2 }}</td>
                                <td>{{ $raffle->number_3 }}</td>
                                <td>{{ $raffle->datetime }}</td>
                                @if(auth()->user()->is_role(\App\User::ADMIN))
                                <td>
                                    <a href="/raffles/{{ $raffle->id }}" class="btn btn-primary btn-sm">
                                        <i class="fa fa-eye"></i>
                                        Ver ganadores
                                    </a>
                                </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
