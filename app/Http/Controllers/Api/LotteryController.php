<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Lottery;
use Illuminate\Http\Request;

class LotteryController extends Controller
{
    public function index()
    {
        $lotteries = Lottery::where('status', true)
            ->get();

        $data = [];

        foreach ($lotteries as $lottery) {
            $data[] = [
                'id' => $lottery->id,
                'name' => $lottery->name,
                'abbreviated' => $lottery->abbreviated,
                'code' => $lottery->code,
                'active' => $lottery->active,
            ];
        }

        return $data;
    }
}
