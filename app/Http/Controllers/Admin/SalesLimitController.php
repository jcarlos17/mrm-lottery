<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\SalesLimit;
use App\SalesPlayLimit;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SalesLimitController extends Controller
{
    public function index(): View
    {
        $limits = SalesLimit::whereNull('user_id')->get();

        $weeklySalesLimits = SalesLimit::whereNotNull('user_id')
            ->where('lottery_level', 'all')
            ->where('frequency', 'weekly')
            ->get();

        $dailySalesLimits = SalesLimit::whereNotNull('user_id')
            ->where('lottery_level', 'each')
            ->where('frequency', 'daily')
            ->get();

        $globalPlayLimits = SalesPlayLimit::where('type', SalesPlayLimit::GLOBAL)->get();
        $sellerPlayLimits = SalesPlayLimit::where('type', SalesPlayLimit::SELLER)->get();
        $ticketPlayLimits = SalesPlayLimit::where('type', SalesPlayLimit::TICKET)->get();

        return view('admin.sales-limit.index', compact(
            'limits', 
            'weeklySalesLimits', 'dailySalesLimits',
            'globalPlayLimits', 'sellerPlayLimits',
            'ticketPlayLimits'
        ));
    }

    public function update(Request $request): RedirectResponse
    {
        // SalesLimit
        $this->updateGlobalLimitsByType($request);
        $this->updateWeeklySellerLimitsByType($request);
        $this->updateDailySellerLimitsByType($request);

        // SalesPlayLimit
        $this->updateGlobalPlayLimits($request);
        $this->updateSellerPlayLimits($request);        
        $this->updateTicketPlayLimits($request);

        return redirect()->to('sales-limit');
    }

    /**
     * On the database:
     * - The limit with id 1 represents global limits across all sellers and lotteries, validated for current date.
     * - The limit with id 2 represents limits per seller across lotteries, validated for current week.
     */
    private function updateGlobalLimitsByType(Request $request)
    {
        $limitIds = $request->limit_ids;

        if (!$limitIds) return;

        foreach ($limitIds as $key => $limitId) {
            $salesLimit = SalesLimit::findOrFail($limitId);
            $salesLimit->update([
                'quiniela' => $request->quiniela[$key],
                'pale' => $request->pale[$key],
                'super_pale' => $request->super_pale[$key],
                'tripleta' => $request->tripleta[$key],
            ]);
        }
    }
    
    private function updateWeeklySellerLimitsByType(Request $request)
    {
        $sellerIds = $request->weekly_seller_ids;
        
        if (!$sellerIds) {
            SalesLimit::whereNotNull('user_id')
                ->where('frequency', 'weekly')
                ->delete();
            return;
        }

        SalesLimit::whereNotNull('user_id')
            ->whereNotIn('user_id', $sellerIds)
            ->delete();

        foreach ($sellerIds as $key => $sellerId) {
            SalesLimit::updateOrCreate(
                [
                    'user_id' => $sellerId,
                    'frequency' => 'weekly',
                    'lottery_level' => 'all'
                ], [
                    'quiniela' => $request->weekly_quiniela_seller[$key],
                    'pale' => $request->weekly_pale_seller[$key],
                    'super_pale' => $request->weekly_super_pale_seller[$key],
                    'tripleta' => $request->weekly_tripleta_seller[$key]
                ]
            );
        }
    }

    private function updateDailySellerLimitsByType(Request $request)
    {
        $dailySellerIds = $request->daily_seller_ids;

        if (!$dailySellerIds) {
            SalesLimit::whereNotNull('user_id')
                ->where('frequency', 'daily')
                ->delete();
            return;
        }

        SalesLimit::whereNotNull('user_id')
            ->whereNotIn('user_id', $dailySellerIds)
            ->delete();

        foreach ($dailySellerIds as $key => $sellerId) {
            SalesLimit::updateOrCreate(
                [
                    'user_id' => $sellerId,
                    'frequency' => 'daily',
                    'lottery_level' => 'each'
                ], [
                    'quiniela' => $request->daily_quiniela_seller[$key],
                    'pale' => $request->daily_pale_seller[$key],
                    'super_pale' => $request->daily_super_pale_seller[$key],
                    'tripleta' => $request->daily_tripleta_seller[$key]
                ]
            );
        }
    }
    
    private function updateGlobalPlayLimits(Request $request)
    {
        $globalPlayLimitIds = $request->global_play_limit_ids;
        
        if (!$globalPlayLimitIds) {
            SalesPlayLimit::where('type', SalesPlayLimit::GLOBAL)->delete();            
            return;
        }

        SalesPlayLimit::where('type', SalesPlayLimit::GLOBAL)
            ->whereNotIn('id', $globalPlayLimitIds)
            ->delete();

        foreach ($globalPlayLimitIds as $key => $globalPlayLimitId) {
            SalesPlayLimit::updateOrCreate(
                [
                    'id' => $globalPlayLimitId
                ], [
                    'number' => $request->global_numbers[$key],
                    'points' => $request->global_points[$key],
                    'type' => SalesPlayLimit::GLOBAL
                ]
            );
        }            
    }
    
    private function updateSellerPlayLimits(Request $request)
    {
        $sellerPlayLimitIds = $request->seller_play_limit_ids;
        
        if (!$sellerPlayLimitIds) {
            SalesPlayLimit::where('type', SalesPlayLimit::SELLER)->delete();
            return;
        }

        SalesPlayLimit::where('type', SalesPlayLimit::SELLER)
            ->whereNotIn('id', $sellerPlayLimitIds)
            ->delete();

        foreach ($sellerPlayLimitIds as $key => $sellerPlayLimitId)
            SalesPlayLimit::updateOrCreate(
                [
                    'id' => $sellerPlayLimitId
                ], [
                    'number' => $request->seller_numbers[$key],
                    'points' => $request->seller_points[$key],
                    'type' => SalesPlayLimit::SELLER
                ]
            );
    }
    
    private function updateTicketPlayLimits(Request $request)
    {
        $ticketPlayLimitIds = $request->ticket_play_limit_ids;

        if (!$ticketPlayLimitIds) {
            SalesPlayLimit::where('type', SalesPlayLimit::TICKET)->delete();
            return;
        }

        SalesPlayLimit::where('type', SalesPlayLimit::TICKET)
            ->whereNotIn('id', $ticketPlayLimitIds)
            ->delete();

        foreach ($ticketPlayLimitIds as $key => $ticketPlayLimitId) {
            SalesPlayLimit::updateOrCreate(
                [
                    'id' => $ticketPlayLimitId
                ], [
                    'number' => $request->ticket_numbers[$key],
                    'points' => $request->ticket_points[$key],
                    'type' => SalesPlayLimit::TICKET
                ]
            );
        }
    }
}
