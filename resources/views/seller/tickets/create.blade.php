@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="" method="POST">
            @csrf
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-7">
                            <h5>Registrar ticket</h5>
                        </div>
                        <div class="col-sm-5">
                            <div class="float-right">
                                <button type="button" data-register="{{ url('api/tickets') }}" class="btn btn-sm btn-primary">
                                    <i class="fa fa-save"></i>
                                    Registrar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div id="general-alert"></div>
                    <div class="row">
                        <div class="col-sm-3">
                            <h5>Loterías</h5>
                            @foreach($lotteries as $lottery)
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="lotteries[]" value="{{ $lottery->id }}" id="lottery{{$lottery->id}}"
                                    {{ $lottery->active ?: 'disabled' }}>
                                    <label class="form-check-label" for="lottery{{$lottery->id}}">
                                        {{ $lottery->name }}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                        <div class="col-sm-9">
                            <div id="alert"></div>
                            
                            @if (is_array($ticketPlays) && sizeof($ticketPlays) === 0)
                            <div class="form-inline mb-4 justify-content-end">
                                <div class="btn btn-secondary mr-4" id="btnCombineQuinielas">
                                    Combinar <i class="fa fa-book"></i>
                                </div>
                                <div class="btn btn-secondary" id="btnCopyTicket">
                                    Copiar ticket <i class="fa fa-copy"></i>
                                </div>
                                <div class="form-group mx-sm-3 mb-2" id="formGroupTicketCode" style="display: none;">
                                    <label for="ticket_code" class="sr-only">Ticket #</label>
                                    <input type="text" class="form-control" id="ticket_code" placeholder="X_YZ">
                                </div>
                                <button type="button" id="btnSearch" class="btn btn-primary mb-2" style="display: none;">
                                    <i class="fa fa-search"></i>
                                    Buscar
                                </button>
                            </div>
                            <hr>
                            @endif                            
                            <div class="form-inline">
                                <div class="form-group mb-2" style="display: none">
                                    <label for="type" class="sr-only">Jugada</label>
                                    <select name="" id="type" class="form-control">
                                        <option value="">Seleccionar una jugada</option>
                                        <option value="Quiniela">Quiniela</option>
                                        <option value="Pale">Pale</option>
                                        <option value="Tripleta">Tripleta</option>
                                    </select>
                                </div>
                                <div class="form-group mx-sm-3 mb-2">
                                    <label for="points" class="sr-only">Puntos</label>
                                    <input type="number" class="form-control" id="points" placeholder="Puntos">
                                </div>
                                <div class="form-group mx-sm-3 mb-2">
                                    <label for="number" class="sr-only">Número</label>
                                    <input type="number" class="form-control" id="number" placeholder="Número">
                                </div>
                                <button type="button" id="btnAdd" class="btn btn-primary mb-2">
                                    Marcar
                                </button>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Jugada</th>
                                            <th>Puntos</th>
                                            <th>Número</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody">
                                    @foreach($ticketPlays as $play)
                                        <tr id="{{ 'tr'.$play->type.$play->number }}">
                                            <td>{{ $play->type }}<input type="hidden" name="type[]" value="{{ $play->type }}"></td>
                                            <td>{{ $play->points }}<input type="hidden" name="points[]" value="{{ $play->points }}"></td>
                                            <td>{{ $play->number }}<input type="hidden" name="number[]" value="{{ $play->number }}"></td>
                                            <td>
                                                <button class="btn btn-sm btn-danger" type="button" title="Eliminar" data-delete="">
                                                    <i class="fa fa-trash o"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td>
                                            <strong>Total</strong>
                                        </td>
                                        <td id="totalPoints">0</td>
                                        <td colspan="2"></td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script src="//unpkg.com/sweetalert2@7.3.0/dist/sweetalert2.all.js" defer></script>
    <script>
        $(document).ready(function(){
            $(".form-control").keypress(function(event) {
                if (event.keyCode === 13) {
                    const textboxes = $("input.form-control");
                    const currentBoxNumber = textboxes.index(this);
                    if (textboxes[currentBoxNumber + 1] != null) {
                        const nextBox = textboxes[currentBoxNumber + 1]
                        nextBox.focus();
                        nextBox.select();
                        event.preventDefault();
                        return false
                    }
                }
            });

            $("#number").keypress(function(event) {
                if (event.keyCode === 13) {
                    $('#btnAdd').focus();
                }
            });
            
            $('#btnCopyTicket').click(() => {
                $('#formGroupTicketCode').show();
                $('#btnSearch').show();
                $('#btnCopyTicket').hide();
            });
            
            $('#btnCombineQuinielas').click(combineQuinielas);
            
            $('#btnSearch').click(() => {
                location.href = '/tickets/create?copyTicket=' + $('#ticket_code').val();
            });
        });
        
        function combineQuinielas() {
            const trElements = document.querySelectorAll('tr[id^="trQuiniela"]');
            
            if (trElements.length < 2) {
                setPlayWarningMessage("Se necesitan al menos 2 Quinielas marcadas para generar combinaciones.");
                return;
            }
            
            const pointsStr = prompt('Cuántos puntos desea marcar?');
            const points = parseInt(pointsStr);
            
            if (isNaN(points)) {
                setPlayErrorMessage('La cantidad de puntos ingresada no es un número válido.');
                return;
            }
            
            if (points <= 0) {
                setPlayErrorMessage('Debe asignar al menos 1 punto para las combinaciones.');
                return;
            }

            // Convert trQuiniela10 to 10
            const numbers = Array.from(trElements).map(tr => tr.id.substring(10));

            // Generate combinations
            const combinations = [];
            const existing = [];
            for (let i = 0; i < numbers.length; i++) {
                for (let j = i+1; j < numbers.length; j++) {
                    const combinedPale = `${numbers[i]}${numbers[j]}`;
                    if (document.getElementById(`trPale${combinedPale}`)) {
                        existing.push(combinedPale);
                    } else {
                        combinations.push(combinedPale);
                    }
                }
            }
            
            if (combinations.length === 0) {
                setPlayWarningMessage("No es posible generar nuevas combinaciones. Ya se encuentran registradas.");
                return;
            }
            
            let successMessage = "Se han generado las siguientes combinaciones: " + combinations.join(', ') + ".";
            
            if (existing.length > 0) {
                successMessage += " Estas ya se encontraban registradas: " + existing.join(', ') + ".";
            }

            for (let i = 0; i < combinations.length; i++) {
                addPlayTableRow('Pale', combinations[i], points);
                updateTotalPoints();
            }

            setPlaySuccessMessage(successMessage);
        }
        
        $(document).on('click', '[data-delete]', function () {
            swal({
                title: '¿Seguro que desea eliminar esta jugada?',
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',

                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sí, eliminar!'
            }).then((result) => {
                if (result.value) {
                    let $div = $(this).closest('tr');
                    $div.remove();
                    updateTotalPoints();
                }
            });
        });
        
        function updateTotalPoints() {
            const sumPoints = $('input[name="points[]"]')
                .toArray()
                .reduce((sum, input) => sum + parseInt($(input).val()), 0);

            const lotteriesCount = document.querySelectorAll('input[name="lotteries[]"]:checked').length;
            
            const totalPoints = sumPoints * lotteriesCount;

            $('#totalPoints').text(totalPoints);
        }

        $('input[name="lotteries[]"]').on('change', function() {
            updateTotalPoints();
        });
        
        const $number = $('#number');
        const $type = $('#type');
        let $points = $('#points');
        
        $(document).ready(function() {
            $('#btnAdd').on('click', onAddPoints);
        });

        $number.on('keyup', function () {
            let length = $(this).val().length;
            if (length === 6) {
                $type.val('Tripleta');
            } else if (length === 4) {
                $type.val('Pale');
            } else if (length === 2) {
                $type.val('Quiniela');
            } else {
                $type.val('');
            }
        });

        function onAddPoints() {
            let $selectedTypeOption = $type.find('option:selected');
                        
            const numberStr = $number.val();
            const type = $selectedTypeOption.val();
            const pointsStr = $points.val();
            const lotteries = $('[name="lotteries[]"]:checked').map((_, el) => el.value).get();
            
            if (numberStr === '' || type === ''|| pointsStr === '') {
                setPlayErrorMessage('Complete los campos para agregar una cuenta.');
            } else if (pointsStr <= 0) {
                setPlayErrorMessage('Ingrese puntos mayor a 0.');
            } else if (type === 'Quiniela' && numberStr.length !== 2) {
                setPlayErrorMessage('Ingrese un número de 2 dígitos para la jugada Quiniela.');
            } else if (type === 'Pale' && numberStr.length !== 4) {
                setPlayErrorMessage('Ingrese un número de 4 dígitos para la jugada Pale.');
            } else if (type === 'Tripleta' && numberStr.length !== 6) {
                setPlayErrorMessage('Ingrese un número de 6 dígitos para la jugada Tripleta.');
            } else if (lotteries.length === 0) {
                setPlayErrorMessage('Por favor seleccione primero las loterías.');
            } else {
                updatePlayAlert('');
                const points = parseInt(pointsStr);

                const $repeatTr = $('#tr'+type+numberStr);

                if ($repeatTr.length) {
                    if (confirm('La jugada ('+type + ' ' + numberStr+') ya se encuentra marcada. ¿Desea eliminarla para volver a registrarla?')) {
                        const $pointInput = $repeatTr.find('[name="points[]"]');
                        const $targetTr = $pointInput.closest('tr');
                        $targetTr.remove();
                        updateTotalPoints();
                        // TODO: Remove tr and call onAddPoints again, so it checks for the limits
                        /*
                        const $pointInput = $repeatTr.find('[name="points[]"]');
                        const $targetTd = $pointInput.closest('td');
                        const oldPointsValue = parseInt($pointInput.val());
                        const newSum = oldPointsValue + points;
                        $targetTd.html(newSum + '<input type="hidden" name="points[]" value="'+newSum+'"></td>');*/
                    }
                    // clearAfterPlayAdded();
                } else {
                    // Before adding, let's check if limits would be exceeded
                    const $token = @json($tokenResult->accessToken);

                    $.ajax({
                        type: 'GET',
                        url: '/api/tickets/limits/check/daily',
                        headers: {
                            'Authorization': 'Bearer '+ $token
                        },
                        data:{
                            '_token':$('input[name=_token]').val(),
                            lotteries,
                            type,
                            'number': numberStr,
                            points
                        },
                        success: function (data) {
                            if (data.success) {
                                // Add as it is
                                addPlayTableRow(type, numberStr, pointsStr);
                                clearAfterPlayAdded();
                            } else {
                                if (data.available <= 0) {
                                    // Cannot add more
                                    setPlayErrorMessage(`Lo sentimos, usted ya ha alcanzado su límite de ventas diario para ${type}.`);
                                } else {
                                    // Adjust before adding
                                    setPlayErrorMessage(`Usted ha alcanzado su límite diario en ${type}. Se ha modificado la jugada con su límite permitido de ${data.available} puntos, en vez de ${pointsStr}.`);
                                    addPlayTableRow(type, numberStr, data.available);
                                    clearAfterPlayAdded();
                                }                                
                            }
                        }
                    });
                    
                    clearAfterPlayAdded();
                }
            }
        }
        
        function clearAfterPlayAdded() {
            updateTotalPoints();
            
            $number.val('');
            $type.removeAttr('selected');
            $points.val('');

            $points.focus();
        }
        
        function updatePlayAlert(newHtmlContent) {
            const $alert = $('#alert');
            $alert.html(newHtmlContent);
        }
        
        function setPlayErrorMessage(message) {
            updatePlayAlert(
                getDangerAlertHtml(message)
            );
        }

        function setPlayWarningMessage(message) {
            updatePlayAlert(
                getAlertHtml(message, 'warning')
            );
        }

        function setPlaySuccessMessage(message) {
            updatePlayAlert(
                getAlertHtml(message, 'success')
            );
        }
        
        function addPlayTableRow(type, numberStr, pointsStr) {
            const $tBody = $('#tbody');
            
            $tBody.append('<tr id="tr'+type+numberStr+'">' +
                '<td>'+type+'<input type="hidden" name="type[]" value="'+type+'"></td>' +
                '<td>'+pointsStr+'<input type="hidden" name="points[]" value="'+pointsStr+'"></td>' +
                '<td>'+numberStr+'<input type="hidden" name="number[]" value="'+numberStr+'"></td>' +
                '<td>' +
                    '<button class="btn btn-sm btn-danger" type="button" title="Eliminar" data-delete="">' +
                    '<i class="fa fa-trash o"></i>' +
                    '</button>' +
                '</td>' +
            '</tr>');
        }
        
        function getDangerAlertHtml(message) {
            return getAlertHtml(message, 'danger');
        }
        
        function getAlertHtml(message, type) {
            return `<div class="alert alert-${type} alert-dismissable">` +
                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                '<strong>' + message + '</strong>' +
                '</div>';
        }
        
        $(document).on('click', '[data-register]', function () {
            let urlRegister = $(this).data('register');
            let $tbody = $('#tbody');
            let $trs = $tbody.find($('tr'));
            const $token = @json($tokenResult->accessToken);

            const lotteries = $('[name="lotteries[]"]:checked').map(function() {
                return this.value;
            }).get();

            const plays = [];

            $.each($trs, function(key, tr) {
                let play = {
                    type : $(tr).find('[name="type[]"]').val(),
                    number : $(tr).find('[name="number[]"]').val(),
                    points : $(tr).find('[name="points[]"]').val(),
                };

                plays.push(play);
            });

            $(this).prop('disabled', true);

            $.ajax({
                type: 'POST',
                url: urlRegister,
                headers: {
                    'Authorization': 'Bearer '+$token
                },
                data:{
                    '_token':$('input[name=_token]').val(),
                    'lotteries':lotteries,
                    'plays':plays,
                },
                success: function (data) {
                    if (data.success) {
                        if (data.ticket) {
                            window.location.href = "/ticket/" + data.ticket.id;
                        } else {
                            window.location.href = "/home";   
                        }
                    } else {
                        $('#general-alert').html(
                            getDangerAlertHtml(data.error_message)
                        );

                        $('[data-register]').prop('disabled', false);
                    }
                }
            });
        });
    </script>
@endsection
