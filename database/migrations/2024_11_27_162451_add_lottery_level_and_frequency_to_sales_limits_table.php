<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLotteryLevelAndFrequencyToSalesLimitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_limits', function (Blueprint $table) {
            $table->string('lottery_level')->default('all'); // 'each', 'all'
            $table->string('frequency')->default('weekly'); // 'daily', 'weekly'
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_limits', function (Blueprint $table) {
            $table->dropColumn([
                'lottery_level', 
                'frequency'
            ]);
        });
    }
}
