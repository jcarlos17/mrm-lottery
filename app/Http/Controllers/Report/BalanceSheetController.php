<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\MovementHistory;
use App\Ticket;
use App\TicketPlay;
use App\User;
use App\Winner;
use Illuminate\Http\Request;

class BalanceSheetController extends Controller
{
    public function index()
    {
        /** @var User $user */
        $user = auth()->user();

        $commission = $user->commission ?? User::DEFAULT_COMMISSION;
        
        // Calcular desde el último ajuste
        $lastTransferDate = MovementHistory::where('user_id', $user->id)
            ->where('type', MovementHistory::TYPE_TRANSFER)
            ->latest('created_at')
            ->value('created_at');
        
        // Total vendido en bruto
        $soldTotal = Ticket::where('user_id', $user->id)
            ->when($lastTransferDate, function ($query) use ($lastTransferDate) {
                return $query->where('created_at', '>', $lastTransferDate);
            })
            ->sum('total_points');
        
        // Comisión correspondiente al vendedor
        $commissionPaid = $soldTotal * (100 - $commission) / 100;

        $pendingToPay = Winner::where('paid', false)
            ->where('user_id', $user->id)
            ->sum('reward');
        
        $central = $user->balance + $pendingToPay;
        
        // Últimos movimientos del historial
        $lastMovements = $user->movement_histories()
            ->when($lastTransferDate, function ($query) use ($lastTransferDate) {
                return $query->where('created_at', '>', $lastTransferDate);
            })
            ->get();
        
        return view('report.balance_sheets', compact(
            'user', 'soldTotal', 'commissionPaid', 
            'pendingToPay', 'central', 'lastMovements'
        ));
    }
}
