<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovementHistory extends Model
{
    public const TYPE_TRANSFER = 'transfer';
    public const TYPE_TICKET_SOLD = 'ticket_sold';
    public const TYPE_WINNER_REWARD = 'winner_reward';
    public const TYPE_TICKET_VOIDED = 'ticket_voided';
    
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    protected $fillable = [
        'amount', 
        'description', 
        'user_id',
        'type' // 'transfer', 'ticket_sold', 'winner_reward', 'ticket_voided'
    ];
}
