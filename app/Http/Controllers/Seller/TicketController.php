<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use App\InactiveDate;
use App\Lottery;
use App\Ticket;
use App\TicketPlay;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    public function create(Request $request)
    {
        $user = auth()->user();

        $lotteries = Lottery::where('status', true)
            ->orderBy('code')
            ->get();
        
        $tokenResult = $user->createToken('Personal Access Token');

        $ticketPlays = [];

        // Ticket code to copy, if any, in the query params
        if ($request->has('copyTicket')) {
            $copyTicket = $request->copyTicket;
            
            $ticket = Ticket::withTrashed()
                ->where('code', $copyTicket)
                ->first();

            if ($ticket) {
                $ticketPlays = $ticket->plays()->get([
                    'number', 'points', 'type'
                ]);   
            }
        }

        return view('seller.tickets.create', compact(
            'lotteries', 'tokenResult', 'ticketPlays'
        ));
    }
}
